<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>
    <link href="{{asset('admin_assets/css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('admin_assets/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('admin_assets/vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('admin_assets/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('admin_assets/vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('admin_assets/css/theme.css')}}" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">

                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABAlBMVEX////qWRsqLm8mK23R0twAAGA9QHiIiabDv9IED2P7+f02OXX6+fs6N3gUGmdCP3y2tcjqUwkMFGUhJmvpTgAXHWhNT4Lo5+0dI2rPzNzjkXiop77pVxP69vIgJWsbIGl5ep3hlX+VlLDqWyHtrJ1VUYja1+RYWYiGg6bi4OmBfqXxyMGtrcIADGNiYI/gno/YoZ3mZDFrapWioLvw2NCTj7BwcJjfopjVpaXgelz36eXlzcvjZzvz1MrkraLt3NfasLLginDtqZThbEbjdlLdubXqv7TasKxjXpLtt6X67ujr8v3fgGXdiXTalITfZTvvo4fTubvddVzNnZ2ovNzbb0iu6mmzAAARsElEQVR4nO2dC3ebRtrHJ0ylAEYjiQEBlQyOXKFLJaURkZK4TpzESZrGabO777vf/6vsDJIlLgMMIGO7h39Pe05lEPw0t+fGAECtWrVq1apVq1atWrVq1apVq1atWrVq1apV69Hpp3+UpgxCYaFyS3roajxlEDahwK3WyQOXU5JQnwP5IQuIi5KEqFNuyN+5xLK99IEQir46B807fSKQSqgjKn0n51am6ajIfmCEZw0Uk66jNkgjxO40UfON7jwkwjPE7F9wBNIIbTntO42J9HAIV2xAATZBcUJyJnoghPJST5gjYHovNTMIgag+CEJ56SQAliYEG/sBECq9RMBMQiPry131/gmVppkIWJ4Q9PB9ExptOxlQgCegJOHMvmfCdEABtkAaocNyOsIaOvdLaLRwGmAWIYSzUCtaxBSKQPf1eyU0hHRAAQog1WqDDSv42WaBUCOMc7+EU5gBmEkoqCHCMzJp4VHosHsldGGme1CAED4cQtfJ9n8eNaGlczh4j5lQ5GhBOlmCkoTefRGKiMtFf7yEnICPl3AucQZZIAaPkrAv8fFRQrks4X1Ybf0FL6AAEShJeB92qadyAwoLem+PrQ2H3F2U3BoNJuYkNFehw6onXOcAVD3/FH7CTaPRaLmhw45HKE9dSxRFy3JTvdJ1ji6qrrfn8BOyLngEQsWdT9pooapIRYimshatlSeyY0SzPICT3UmlCCdmScKpt3Ik3QwvbxDrSBqtrSilskkIi7KEbgHLEW5KxWkMr4f0vYsHfR0wHbU9CY8JF3KaMkT6Zn9aKcIxLE4oLh0fD2LSZEh3cKvdbgsmTZaYW1JoouY8dM7cTo4chuQsAxcqQWjQ6xUiVDot1cezVdjbzC133yPlqdWZjQV16ztghLxgZ1XWepZf7wOOA+eUIeyggoRiU6UXsNXWmcicOw1rPZJ8SIgETwn8xR1lD0azF/xVyhD6R+UnnC5p+0GnsbGU5KMUd+2oW8a2GPzDupExGnE7NEeVIFz7CZHchGuJ8unCMDseK46Q35fVZfBYy0ztqbgVnoSLEw63i1NOwmmP/i76iZfSfAFZY78dsRpsxmkzJQ6MhcgvV5DQ6PR24yEfYYf+/hgNM7M+hxtsI78ZJ4HP5FFisgLCaCCbn9Ca32q96R2WplyEE9IiUB1n98+AlKE/8PTg9KEkIULkRs/PYZcu9il9jA+DPQehQnO1GOefmka0Z2M90DpyjzkWoRoDzOlbMMRP6Odq9VGuBtxKmdAxD53A/RgtxowKEWNcVUeojImBIM34ZpioOrSnhhBdhqsfmo9ulULohPMWZ+xsOTchBdy5bAXk2nSNkQKdsB9zNNQ568yU/OE63J86y1YZwpVDRkkJP8RoUkQUGIubSKeS+swTkwn12KAdsuxeTsIJIrfH6kTc2iK2Dj+7IYSGYlL/qIiQdqn0Fpxa4tzzOqKVmJj104V27/DBPGiiqsOE0/IQrgsTuo2kUbL9szeiHj6t0yJ+vv7VY1NOqVelzw4fjA5LBpowTwEVtaFM+hNaJ/zR8FqSn0iC2DQxhcD6oumxFhU/4bQ49PXDfIpmjMO3qqQNN45gLtl/MmYmvW0bLZzR6uxsM24tqG8IdWfGaMi5SlNmB/jlrhEDLn1MeQgnrBWRg1BUyQzBtkTXOs0sq2jWMW4XSkUctiik6Uzi55DfSjAPOLtGdBJ+vu3V87RhMUKF9lGmEW8JZI019VXsj9N1i7SsY8Yn3zbtp4fj/UY0x7HDAqqgDYkf6TAHobeAxM9g9UYydD2BLKBS7DyXmrbNw+2r9H9T7aSU2kRPFC3ROshdsazdTEJD2ubxYtrQFWQcN5Vvz5tIpO2X0bunfvdh3ZGbMOLSx5RmtfkRWn8a34ppzmcSkpaXGEs9rbiDiG2G7GQRS80ZRe5fISa3n5/faohOMkz5O6/zNjDEjImAenh2O7EBt5LHumBGETtq8KJTJ8tXuXPCoS7EvVIyRTiRkBhTykaPT5Tk5uDBssmsTbtrQtKpWE04oY3D40gRxKi5QmeXzGB14PA7JiS3w1gp5pKAe3ye4jI+jMnd4RX7aNYdpMw0WU88IQ7CJY5UilFNiQkAOX19uYmDDgVVn1xY5Q4VpKyHHSNDP2XHvI0FfWwoKmIxs6ZXtlxHcML9lExegs7tSycTss2Q0JWyq6A9VWjEeuO8Ac0UQzL2HbqghueTlb190IBLd0wojtX4PGN4J4s88ag2jAw7+iBEg/cb7piQdDLWmidnLIRhkdkqzCPbkD8+dOeEx1ATOmEPnhjcNm8/fxSEHeQ/V3DQXN/W4fPoURDK0Wi9KxHPILvS3tejIAQbO7I6qDz3t9XjIOygyGw6Iitiql9yEB+h4it6VHWEUweGvcCnpmAmRtfC4iKcOzbGi9hvNterIlROIAqtF8QKwJyTKRchTRbGLWUZF8vjF1EPS6GppoMElsvCEhchRYn7K9u8QTWESxxOCbjEdud0TngJ433CWnB5wMfRDKOQAU8sepgRn7kVF6EtCHFDcudDVkRoh10UMsnBkyMSmoIeC+t5hSoViooQhiY6Stjis735CGEr2uenOp+PfyStIuPwyISyGU+MrexKCXtQ/Sn4/5Sw/Dg8fCWKhc3FfTVyNYRNGA5b0DBzsywhXs1uJUXdOeWQfq3GpsEwXOo0JYSp2YqDUiJR2N4pnpybHKoWKiG01Mj6Trx83nAbRzQR6tHu4AbKMiohnJgRK5QYjPYZ37kchPHkwhhXS6iMYKRUxnMEJylxH1E2YSCCvtM8WMlSBaGrRsOrZza7PIihbMKYnyiHnuqognBiRgddGwoOZzQrk9CODehw+VcFhDKKNpiyOEacZqeYQWqpuz8G7NJfP/ycV8+i+tvXc1+vXr367bffXr9+/f79+3+DtQNt8Hx/2vP3iqVS5+k9OeD1b75eUW3P9b9m+5U/f3iZTahHx7PSdEybOMS3D0f4hL9o3SNJi+lnYr+onbeHK2gvaFWIPj8dxA8mCn3br5mEcYPUa1jT6dTPW7j2gfDJHWlwqWxs2AQvBvuPtGfgBAqL6cvsi3IQxgxSAx3yEIZ594TaBVncVetj4ALaFTH7oQA+HoMQxyyjJa6UUPt96kB9EmyuwTUtziKr4adB8nnchLH8dEcVpAoJtc83DsTN00AfJZ/R25OmpxzXzCQ0owapTAZAhYTdd+AEQ9P43j18NjgHdCYdgy/HIFxEDVK/mKU6wsGPK68hWaFv1/6g+1OhOXjTTTyPmzBmkPobQ1VI+GTQfSuKb4NfPnihUM+pJRs8l8wgjKdZ/XrOKgnJPX58OQjOKGQUkiYk88zzIxDGipb7C4yx06qU8IkW+urBtWI0yEUN5Tx7Js0ijHmY8tevy+XXTaDQugrCyC1/BitMCwHfcV0xndDJrHaonrD7hk6kgi6f8h2fQZjpYVZPqF3RvBrywDO+Cz46Qu1vGoiGTfmC83qPjZCsFDRlSXzFa55p5kmlhAPOW8q63zGmjuGfvD9odYTd688cFkiGut/9Z0gWxjfuUyojHJyf3vAYWRmEikG8bjTksteqJSSA3tlV2aFKl0KT7onH3UcrIxycX8wb9s33co1I+miHGKTO9FuOn6qaFX/QvRJVQe/zzvAJ33JuyA4U1D74lOOXyrLa+p6v/u4B505Uc8xDqF1ZCArQ5LOVE7/lM03omat8C1OG5Y31sOJbggschNpbd+FXHK/BdfF+SudRWlgM8g1njkhUtjIItS/T3YPtaHpVnHCwnUctcJlrYa2AUPuyf9IT98Dnov10O4+ShYLTHq2OUHt3c7LPU+kz8HcxRO0v+hCqPQa/5jz/zgm1j3I7kIiTPPBXEURqjzbIXCV/43J78xNCbJv0lQgmzQcTLx9DTkLtmRLe+mAxL4So/U731V6IOYyZHIS2uhC+zp4Oh5PJZDbbbDar1deWJB32R00m1D6AceR5PrKc5e+oW59Jn3D69fkIsbNhbizm9tu3W38kEmrfQXzHe2kN/ujm62qDH36AtJdzoeAjNHvJlcad3V7aSYTaG/rMUnzYLm+ufuS61e4VOIHEWgsmZ45FiGP57aD8HQASCbuflAlzRyezZSl/a/wDSnsHzhxBmoOfi4zgDMKMR/sMCBMJu5fKMOm1GmgjX33SOBuk+4nmSpwVVxA/LyH74d2A5iiJsPtC8ZL3cDTJhPP7NV87EqfQhLAtXxSyhzIIGbvZRBoRQzbh4MXpPHUfVdTuyL9/72Y35DZw4bgFbdp0QhjdUiqu/dsfwoSDJxdixi6cEDW9m4tfLrV0SGLMkM6OPJ5saAHC7Mq48S1haKUiDqGVvcMh1PWNBS5eXaa05OCS7qhhL8HbggZthn+YOpMmEw60K5dnw3RqTQhrV/72Z2JssHslE48Jy6d5rbU94ds8hMZkuYq4/UxCPzHNA0gb0lRbM/CvhDFGFoqNSUvoC0exchHKgo6xGs7WsAi1LzfZLy0IMCI3KbJEFgri9SLeLExpwu12N+HnABmEBDDrvRohQMdNGmOD89PpgqazS0TpchH2fRMsXEsbJ9TehfylTEDJepnUA8ks0cYQGUo+t/6OCbXnoJkLULxIivdrz8DMoTtyF7HW7oyQ+DlRfylVqvgtydEgHgWx1vRZMWutDGHI2YgQkp99lfQKNCZgJ9lp1y6ItYZHoFyUtQAh3kwCasIgIfF4Zzl2ayY98DTRIdI+07II3QU/SiWtChAK2Axo71tQQuIQTnLsty0s+uAycZb5C6wR3WIuZ2ztGIQx3RKS1SvPjuk0oJEISKw14tabq9yxtRhhan0pgxDvQlE7wQBh9zrXWwto3C05AaFdyC0IT+SLsnnVvIR4s/FDUavVkmg8HgcIL5V5LsAheJMSJ6fWGrLKpAGKEeoRd0pu72eacyXLXwp30XUa4IedtVbQZSpDGIlLHQj/bVh5AMndf0+8++4PMG3QTGjZQXgMQn+18GvDeN6gdQCcbA2VgcYyaLoX5PLQMXIHuI9AGK2nPRC6Zh7Ajb8IDLQX707jJhlZCYm1JnVyZUITCa/KESq9W8I81ra+8gG167fgxgVPIg1FDL++SgPc/zpGiU7ZNgwQ8jehs/JnEOLdupsGlCOuES1cW9BLH2EQHoHQ33LaJxxxEzrLbdiKoAwlusnjH0GU7g/FsKGgGt+ibVuQsGQvBfkJndHeD/kCoF958PkQOO0ODPqdDbH8SliM0F7Pw3XRe8KvnOPQDBSgD77RvTnVDrj4tH3QRdO+KzSZg4Ypa8ndEgqm3gh9Ns5JaPYC7h6x8/zdhYcAXL3+6z//+f7qCshjhw7UHDVBRyYUhATCJReh3Q5NIMSd7NPtLXv7auvOiU0vWzQ6eu+EdlsO37v2C7AkU4DoZDMXxc7sRIeC3ZITAxv3QLi8JWTubBoRbt5En8UirShvkO2//wghMolCNM6frE9Rt0pCfHITD6tpP16SZVFFDoYQm47U8sDFi+NMo1vCi/yEoYf1VreEm9SXR1NBbFwx1riB9uEK3HSGq9FodOZZCvjzOAvhLaGRhxDF23BPOMt89zA0Lthhta52+efL3fddfDw/bjl1LkLDcSBehJ+K3xMy91AOAU6T5w/6KOiLT28+XXYzcm13TAimk94qso0l3f6DhxAi91t6ncGA6rh0+QkZ2hM+TSWEC/f0mPPHsQjjOw3ECTEPIdR/Uu4HMIswe/+XPSHznQL7LmopRzKkc2uQSijgzDw+F2HDOoq7fheE2fst730LL5mQFtxV9WRbnPAmlTBzh1B/r6gtYWJCBnWO5godnzBzb0h/jfAJk0L+1P0rWDVbCSHEqTVD27KubRsiyFSjD/5PGzBUEWAWIX2fW1JxoiLuXrW4bUP2ODQ98P//vf5xeflir3OWUm9ycPsv+Y//T9JxLGURknuUhNXTYVRPV21pX1/q+6/878LL1OkxBTiqoKlXE5UZfLNcqZc4VaDylez/fMLObofaB6ryhELrrtUup5bDIsSNf5LOjtvta9WqVatWrVq1atWqVatWrVq1atWqVatWrfvX/wCw9XeJOMnv6gAAAABJRU5ErkJggg==" alt="pzg-logo">
                        <br>    {{Config::get('constants.site_name')}} </br>

                        </a>

                        </div>
                        <div class="login-form">
                            <form action="{{route('admin.auth')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="au-input au-input--full" type="email" name="email" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="au-input au-input--full" type="password" name="password" placeholder="Password" required>
                                </div>



                                <div class= "mb-3 mt-4">
                                <label for="level">Pilih Level :  </label>
                                <select name="level" id="level" type="level" placeholder="Level" required>
                                <option value="0">Pilih Disini...Boss</option>
                                  <option value="admin">Admin</option>
                                  <option value="staff-iklan">Staff-Iklan</option>
                                  <option value="staff-service">Staff-Service</option>
                                  <option value="staff-product">Staff-Product</option>
                                  <option value="staff-coupon">Staff-Coupon</option>
                                </select></div>

                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">LOGIN</button>

<a href ="{{ route('google.login') }}" class= "btn btn-block btn-danger"><i class ="fab fa-google-plus mr-2 "></i>Sign in using Google+</a>

                                @if(session()->has('error'))
                                <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show mb-3 mt-4">
                                    {{session('error')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif
                            </form>



                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="{{asset('admin_assets/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('admin_assets/vendor/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{asset('admin_assets/vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin_assets/vendor/wow/wow.min.js')}}"></script>
    <script src="{{asset('admin_assets/js/main.js')}}"></script>
</body>
</html>
