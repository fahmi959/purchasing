<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\Customer;
use App\Models\User;
use App\Models\UsersDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

//  User::create([
//     'name' => Str::random(10),
//     'email' => Str::random(10) , '@gmail.com',
//     'email_verified_at' => 1,
//     'password' => Hash::make('password'),

//  ]);

    UsersDetail::factory(1)->create();


    //     Customer::create([
    //         'name' => 'Bangs' ,
    //         'email' => 'bangs@gmail.com' ,
    //         'password' => 'bangs',
    //         'mobile' => '1234512345',
    //         'status' => '1' ,
    //         'is_verify' => '0' ,
    //         'rand_id' => ''
    //     ]);

    }
}
