<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->session()->has('ADMIN_LOGIN' )) {

                return redirect('admin/dashboard');
            }


       else  if ($request->session()->has('STAFF-IKLAN')) {

                return redirect('admin/dashboard');
            }


              else  if ($request->session()->has('STAFF-SERVICE'  )) {

                    return redirect('admin/dashboard');
                }

                else   if ($request->session()->has('STAFF-PRODUCT' )) {

                    return redirect('admin/dashboard');
                }
                else   if ($request->session()->has('STAFF-COUPON' )) {

                    return redirect('admin/dashboard');
                }

                else {
                    return view('admin.login');

                }




        return view('admin.login');



    }

    public function auth(Request $request)
    {
        $email=$request->post('email');
        $password=$request->post('password');
        $level=$request->post('level');



        $result=Admin::where(['email'=>$email,'password'=>$password , 'level'=>$level])->get();


  // $result=Admin::where(['email'=>$email])->first();
            // if ($result) {
                // if (Hash::check($request->post('password'), $result->password)) {


                    if (isset($result['0']->id) ) {


                        if ($level == 'admin') {

                            $request->session()->put('ADMIN_LOGIN', true);


                            return redirect('admin/dashboard');
                        }
                      else  if ($level == 'staff-iklan') {

                            $request->session()->put('STAFF-IKLAN', true);


                            return redirect('admin/dashboard');
                        }
                      else  if ($level == 'staff-service') {

                            $request->session()->put('STAFF-SERVICE', true);


                            return redirect('admin/dashboard');
                        }
                      else  if ($level == 'staff-product' ) {

                            $request->session()->put('STAFF-PRODUCT', true);


                            return redirect('admin/dashboard');
                        }

                        else  if ($level == 'staff-coupon' ) {

                            $request->session()->put('STAFF-COUPON', true);


                            return redirect('admin/dashboard');
                        }

                        else {
                            $request->session()->flash('error', 'Please enter correct password or level');
                            return redirect('admin');
                        }

                    }


                    else {
                        $request->session()->flash('error', 'Please enter correct password or level');
                        return redirect('admin');
                    }








    }






    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function updatepassword()
    {
        $r=Admin::find(1);
        $r->password=Hash::make('fahmi');
        $r->level = ('admin');
        $r->save();

        $r=Admin::find(2);
        $r->password=Hash::make('syafii');
        $r->level = ('staff-iklan');
        $r->save();

        $r=Admin::find(3);
        $r->password=Hash::make('samudra');
        $r->level = ('staff-service');
        $r->save();

        $r=Admin::find(4);
        $r->password=Hash::make('bobo');
        $r->level = ('staff-product');
        $r->save();

    }

}
